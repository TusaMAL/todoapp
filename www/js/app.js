
var app  = angular.module('todoapp', ['ionic', 'firebase']);

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
  
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});

app.config(function($stateProvider, $urlRouterProvider){

  $stateProvider.state('lista', {
    url: '/lista',
    templateUrl: 'templates/lista.html',
    controller: 'IndexCtrl'
  });

  $stateProvider.state('cadastro', {
    url: '/cadastro',
    templateUrl: 'templates/cadastro.html'
  })

  $urlRouterProvider.otherwise('/lista')

})

app.controller('IndexCtrl', function($scope, TarefaService, $firebaseArray){

//  $scope.tarefas = TarefaService.read();

//  var t1 = {
//    nome: "tarefa 1",
//    descricao: "descrição da tarefa 1",
//    concluida: false
//  }
   
//  TarefaService.create(t1);

var ref = firebase.database().ref().child('tarefas');
$scope.tarefas = $firebaseArray(ref);

});

app.factory('TarefaService', function(){
    var tarefas = [ ];

    return {

      read: function(){
        return tarefas;
      },

      create: function(tarefa){
        tarefas.push(tarefa);
      }
    }

    
})

